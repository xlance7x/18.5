﻿#include <iostream>
#include <algorithm>
#include <utility>

class Player
{
public:
	int score;
	std::string name; 
	Player()
	{
		name = "default";
		score = 0;
	}

	void Show()
	{
		std::cout << name << ": " << score << std::endl;
	
	}

	void Enter()
	{
		std::cout << "Введите имя игрока: ";
		std::cin >> name;
		std::cout << "Введите кол-во очков: ";
		std::cin >> score;
	}

	void static bubblesort(Player* l, Player* r)
	{
		int sz = r - l;
		if (sz <= 1) return;
		bool b = true;
		while (b) 
		{
			b = false;
			for (Player* i = l; i + 1 < r; i++)
			{
				if (i->score > (i + 1)->score)

				{
					std::swap(*i, *(i + 1));
					b = true;
				}
			}
			r--;
		}
	}
	
};


int main()
{
	setlocale(LC_ALL, "Russian");
	int size;
	std::cout << "Колличество игроков: ";
	std::cin >> size;
	Player* Players = new Player[size];

	for (int x = 0; x < size; ++x)
	{
		Players[x].Enter();
	}

	Player::bubblesort(Players, Players + size);

	for (auto x = 0; x < size; ++x)
	{
		Players[x].Show();
	}

	
	delete[] Players;
}
